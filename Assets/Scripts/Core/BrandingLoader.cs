﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class BrandingLoader : MonoBehaviour {

  public string brandingName;

  public string brandingParameter;

  void Start() {
    string[] args = System.Environment.GetCommandLineArgs();
    int argLength = args.Length;
    
    for (int i = 0; i< argLength - 1; i++) {
      if (args[i].CompareTo(brandingParameter) == 0) {
        brandingName = args[i + 1];
        break;
      }
    }


    if (brandingName != null && brandingName != "") {
      Debug.Log("Branding as " + brandingName);
      SceneManager.LoadScene(brandingName, LoadSceneMode.Additive);
    }
  }
}
