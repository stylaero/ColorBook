﻿using UnityEngine;
using System.Collections;

public class EventSystemScript : MonoBehaviour {
  public delegate void VoidDelegate();
  public delegate void StringDelegate(string s);
  public delegate void ScreenshotDelegate(string screenshotname, string filepath);
  public delegate void Vector3Delegate(Vector3 v);

  public event VoidDelegate OnScreenshotStart;
  public event ScreenshotDelegate OnScreenshotEnd;

  public event VoidDelegate OnCalibrationStart;
  public event VoidDelegate OnCalibrationEnd;

  private static EventSystemScript instance;

  void Awake() {
    instance = this;
  }

  public static EventSystemScript Instance {
    get {
      return instance;
    }
  }

  public void CalibrationEnd() {
    if (OnCalibrationEnd != null)
      OnCalibrationEnd();
  }


  public void CalibrationStart() {
    if (OnCalibrationStart != null)
      OnCalibrationStart();
  }

  public void TakeScreenshotStart() {
    if (OnScreenshotStart != null)
      OnScreenshotStart();
  }

  public void TakeScreenshotEnd(string screenshotName, string filepath) {
    if (OnScreenshotEnd != null)
      OnScreenshotEnd(screenshotName, filepath);
  }
}
