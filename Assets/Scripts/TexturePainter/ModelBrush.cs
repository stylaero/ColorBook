﻿using UnityEngine;
using System.Collections;

public class ModelBrush : MonoBehaviour {
  public EventSystemScript.Vector3Delegate OnBrushImpact;

  public GameObject PaintPrefab;
  public GameObject SpecularPaintPrefab;
	
  public float minimalPaintDistance;
  public float pencilLength;
  public float MaxSize;
  public float Slope;

  public int maxIterpolations;

  private Vector3 lastPos;
  private Vector3 lastDirection;
  private SoundOnTouch lastTouch;
  public PaintBrushColor brushColor;
  public Material BrushMaterial;
  public float SpecularAmount;

  public bool IsPainting;
  public float BrushSpeed;

  private float brushLength;

  private PaintBrushParticles particleBrush;

  void Start() {
    GameSettings settings = FindObjectOfType<GameSettings>();
    settings.OnSettingsChanged += (GameSettings.Settings currentSettings) => {
      brushLength = currentSettings.pencilLength;
    };
    brushLength = settings.getCurrentSettings().pencilLength;
    particleBrush = GetComponent<PaintBrushParticles>();
  }

  public PaintableMesh PaintBrushTarget(Ray ray, float maxLength, out RaycastHit hit) {
    if (Physics.Raycast(ray, out hit, maxLength)) {
      PaintableMesh paint = hit.transform.GetComponent<PaintableMesh>();
      return paint;
    } else {
      return null;
    }
  }

  private void Paint(Vector3 position, Vector3 forward, Quaternion BrushRotation) {
    Vector2 uvHitPosition;
    Debug.DrawRay(position, forward * pencilLength, Color.red);
    Ray cursorRay = new Ray(position, forward);
    RaycastHit hit;
    PaintableMesh paintableMesh = PaintBrushTarget(cursorRay, pencilLength, out hit);
    if (paintableMesh != null) {
      uvHitPosition = hit.textureCoord;
      Quaternion rotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));

      float size = (pencilLength - hit.distance) * Slope * brushLength;
      size = Mathf.Min(size, MaxSize * brushLength);

      // Main texture
      TexturePainter painter = paintableMesh.GetTexturePainter(TexturePainter.MaterialType._MainTex);
      if (painter) {
        GameObject paint = PaintPrefab.Spawn(
          painter.UVPositionToRenderTexturePosition(uvHitPosition),
          rotation);
        paint.GetComponent<SpriteRenderer>().color = brushColor.BrushColor;
        paint.transform.localScale = size * Vector3.one;
        painter.AddStroke(paint);
      }

      // Specular texture
      painter = paintableMesh.GetTexturePainter(TexturePainter.MaterialType._SpecGlossMap);
      if (painter) {
        GameObject paint = PaintPrefab.Spawn(
          painter.UVPositionToRenderTexturePosition(uvHitPosition),
          rotation);
        Color c = brushColor.BrushColor;
        c.a = SpecularAmount;
        paint.GetComponent<SpriteRenderer>().color = c;
        paint.transform.localScale = size * Vector3.one;
        painter.AddStroke(paint);
      }

      // Particles
      ParticlePaint particlePaint = particleBrush.paintParticles;
      if (particlePaint) {
        ParticlePaint[] particles = paintableMesh.GetComponentsInChildren<ParticlePaint>();
        bool found = false;
        foreach (var p in particles) {
          if (p.SameType(particlePaint)) {
            p.addPoint(hit);
            found = true;
            break;
          }
        }
        if (!found) {
          var p = paintableMesh.addParticlePainter(particlePaint);
          p.addPoint(hit);
        }
      }
    }
  }

  public Vector3[] Interpolate(Vector3 startPos, Vector3 endPos, int nPoints) {
    Vector3[] pos = new Vector3[nPoints];
    for (int i = 0; i < nPoints; i++) {
      pos[i] = Vector3.Lerp(startPos, endPos, i / (float)nPoints);
    }
    return pos;
  }


  public void CheckHitModel(Vector3 pos, Vector3 forward) {
    Ray cursorRay = new Ray(pos, forward);
    RaycastHit hit;
    if (Physics.Raycast(cursorRay, out hit, pencilLength)) {
      SoundOnTouch touch = hit.transform.GetComponent<SoundOnTouch>();
      if (touch != lastTouch) {
        if (lastTouch)
          lastTouch.Release();
        IsPainting = false;
        if (touch) {
          touch.Impact();
          IsPainting = true;
        }
        lastTouch = touch;
      }
      if (touch)
        touch.Contact(hit.point, BrushSpeed, Time.deltaTime);
    } else {
      if (lastTouch)
        lastTouch.Release();
      lastTouch = null;
      IsPainting = false;
    }
  }

  void Update() {
    BrushMaterial.color = brushColor.BrushColor;

    BrushSpeed = (transform.position - lastPos).magnitude / Time.deltaTime;
    CheckHitModel(transform.position, transform.forward);

    float distance = (lastPos - transform.position).magnitude;
    int nPoints = (int)(distance / minimalPaintDistance);
    nPoints = Mathf.Clamp(nPoints, 1, maxIterpolations);

    Vector3[] positions = Interpolate(lastPos, transform.position, nPoints);
    Vector3[] direction = Interpolate(lastDirection, transform.forward, nPoints);
    for (int i = 0; i < positions.Length; i++) {
      Paint(positions[i], direction[i].normalized, transform.rotation);
    }
    lastPos = transform.position;
    lastDirection = transform.forward;
  }
}