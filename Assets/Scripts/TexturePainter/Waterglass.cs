﻿using UnityEngine;
using System.Collections;

public class Waterglass : MonoBehaviour {

  public float WaterAdditionConstant;

  public void InteractHit(Interactor.InteractionHitData data) {
    PaintBrushColor drop = data.InteractorRigidBody.GetComponentInChildren<PaintBrushColor>();
    if (drop) {
      float d = Mathf.Clamp(data.InteractorRigidBody.GetPointVelocity(data.hit.point).magnitude, 0, 2);
      drop.WaterAmount += (d * data.deltaTime * WaterAdditionConstant);
    }
  }
}
