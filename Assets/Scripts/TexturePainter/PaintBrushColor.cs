﻿using UnityEngine;
using System.Collections;

/**
 *  Color model for paint brush
 **/
public class PaintBrushColor : MonoBehaviour {
  public delegate void WaterChanged(float amount);
  public delegate void ColorChanged(Color color);

  public event WaterChanged OnWaterChanged;
  public event ColorChanged OnColorChanged;

  public float MaxWaterAmount;

  private float _waterAmount;
  public float WaterAmount {
    get {
      return _waterAmount;
    }
    set {
      float newWater = Mathf.Clamp(value, 0, MaxWaterAmount);
      float oldWater = _waterAmount;
      _waterAmount = newWater;
      if (newWater != oldWater && OnWaterChanged != null)
        OnWaterChanged(newWater);
    }
  }

  private Color _brushColor = Color.black;
  public Color BrushColor {
    get {
      return _brushColor;
    }
    set {
      Color oldColor = _brushColor;
      _brushColor = value;
      if (oldColor != _brushColor && OnColorChanged != null)
        OnColorChanged(value);
    }
  }

  public Color Mix(Color brushColor, Color bucketColor, float time) {
    Vector4 c1 = ToCMYK(ToCMY(brushColor));
    Vector4 c2 = ToCMYK(ToCMY(bucketColor));
    Vector4 mix = Vector4.Lerp(c1, c2, time);

    Color mixedColor = ToRGB(ToCMY(mix));

    mixedColor.a = Mathf.Lerp(brushColor.a, bucketColor.a, time);
    return mixedColor;
  }

  // {C, M, Y}
  public Vector3 ToCMY(Color c) {
    return new Vector3(1 - c.r, 1 - c.g, 1 - c.b);
  }

  public Color ToRGB(Vector3 cmy) {
    Color c = new Color();
    c.r = (1 - cmy.x);
    c.g = (1 - cmy.y);
    c.b = (1 - cmy.z);
    c.a = 1;
    return c;
  }

  public Vector3 ToCMY(Vector4 c) {
    Vector3 cmy;
    cmy.x = (c.x * (1 - c.w) + c.w);
    cmy.y = (c.y * (1 - c.w) + c.w);
    cmy.z = (c.z * (1 - c.w) + c.w);
    return cmy;
  }

  // {C, M, Y, K}
  public Vector4 ToCMYK(Vector3 cmyColor) {
    float var_K = 1;
    Vector4 cmykColor;
    var_K = Mathf.Min(cmyColor.x, cmyColor.y, cmyColor.z);

    if (var_K == 1) { //Black
      cmykColor = Vector4.zero;
    } else {
      cmykColor.x = (cmyColor.x - var_K) / (1 - var_K);
      cmykColor.y = (cmyColor.y - var_K) / (1 - var_K);
      cmykColor.z = (cmyColor.z - var_K) / (1 - var_K);
    }
    cmykColor.w = var_K;
    return cmykColor;
  }
}