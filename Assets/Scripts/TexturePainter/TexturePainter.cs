﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TexturePainter : MonoBehaviour {

  public int BakeCount;

  public static Vector3 spawnPos = new Vector3(10000, 0, -1000);
  public static Vector3 spawnPosAdd = new Vector3(2, 0, 0);

  public static MaterialType[] AllMaterialTypes = { MaterialType._MainTex, MaterialType._BumpMap, MaterialType._EmissionMap, MaterialType._ParallaxMap, MaterialType._SpecGlossMap };

  public enum MaterialType {
    _MainTex,
    _BumpMap,
    _EmissionMap,
    _SpecGlossMap,
    _ParallaxMap
  }

  public Camera camera;
  public MeshRenderer textureQuad;

  private RenderTexture renderTexture;
  private Material quadMaterial;

  private Material objectMaterial;
  private MaterialType objectMaterialType;

  private Texture startTexture;
  private Texture originalTexture;

  private float paintScale;

  private List<GameObject> brushes = new List<GameObject>();

  public void AddStroke(GameObject s) {
    s.transform.parent = transform;
    s.transform.localScale *= paintScale;
    brushes.Add(s);

    if (transform.childCount > BakeCount) {
      Bake(false);
    }
  }

  public void StartPaint(Material material, MaterialType texture, Texture originalTexture, float paintScale) {
    this.paintScale = paintScale;
    startTexture = material.GetTexture(texture.ToString());
    this.originalTexture = originalTexture;

    //quadMaterial = new Material(textureQuad.material.shader);
    //quadMaterial.SetTexture("_MainTex", org);
    textureQuad.material.mainTexture = startTexture;

    renderTexture = new RenderTexture(startTexture.width, startTexture.height, 24, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
    renderTexture.Create();
    camera.SetTargetBuffers(renderTexture.colorBuffer, renderTexture.depthBuffer);
    material.SetTexture(texture.ToString(), renderTexture);
    objectMaterial = material;
  }

  public void FinishPaint() {
    Bake(false);
    FinalizeMaterial();
    Destroy(gameObject);
  }

  public Vector3 UVPositionToRenderTexturePosition(Vector3 pixelUV) {
    Vector3 uvWorldPosition = new Vector3();
    uvWorldPosition.x = pixelUV.x - camera.orthographicSize + transform.position.x; //To center the UV on X
    uvWorldPosition.y = pixelUV.y - camera.orthographicSize + transform.position.y;//To center the UV on Y
    uvWorldPosition.z = transform.position.z + 1;
    return uvWorldPosition;
  }

  //Sets the base material with a our canvas texture, then removes all our brushes
  public void Bake(bool clear) {
    if (!clear) {
      Texture oldTexture = textureQuad.material.mainTexture;

      camera.Render();
      RenderTexture.active = renderTexture;
      Texture2D tex = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGB24, false);
      tex.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
      tex.Apply();
      RenderTexture.active = null;
      textureQuad.material.mainTexture = tex;
      //quadMaterial.mainTexture = tex; //Put the painted texture as the base
      if (oldTexture != originalTexture)
        Destroy(oldTexture);
    } else {
      textureQuad.material.mainTexture = originalTexture;
    }

    foreach (var brush in brushes)
      brush.Recycle();
    brushes.Clear();
    //StartCoroutine ("SaveTextureToFile"); //Do you want to save the texture? This is your method!
  }

  private void FinalizeMaterial() {
    Texture tex = textureQuad.material.mainTexture;
    /*
    RenderTexture.active = renderTexture;
    Texture2D tex = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGB24, false);
    tex.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
    tex.Apply();
    RenderTexture.active = null;
    */
    renderTexture.Release();
    Destroy(renderTexture);
    objectMaterial.SetTexture(objectMaterialType.ToString(), tex);
    //objectMaterial.mainTexture = tex;
  }
}