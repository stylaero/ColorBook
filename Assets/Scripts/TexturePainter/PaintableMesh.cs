﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PaintableMesh : MonoBehaviour {

  public TexturePainter texturePainterPrefab;

  private Dictionary<TexturePainter.MaterialType, TexturePainter> paintableTextures = new Dictionary<TexturePainter.MaterialType, TexturePainter>();
  private Dictionary<TexturePainter.MaterialType, Texture> orginalTextures = new Dictionary<TexturePainter.MaterialType, Texture>();

  public float PaintScale = 1;

  private bool isBeingPainted;

  public bool IsBeingPainted {
    get {
      return isBeingPainted;
    }
  }

  public bool PaintMainTex;
  public bool PaintSpecGlossTex;
  public bool PaintParallaxTex;

  private TexturePainter CreateTexturePainter(ref Material m, TexturePainter.MaterialType materialKind) {
    TexturePainter p = Instantiate(texturePainterPrefab, TexturePainter.spawnPos, Quaternion.identity) as TexturePainter;
    TexturePainter.spawnPos += TexturePainter.spawnPosAdd;
    p.StartPaint(m, materialKind, orginalTextures[materialKind], PaintScale);
    return p;
  }

  public void StartPaint() {
    isBeingPainted = true;
    Material m = GetComponent<Renderer>().material;
    if (PaintMainTex)
      paintableTextures[TexturePainter.MaterialType._MainTex] = CreateTexturePainter(ref m, TexturePainter.MaterialType._MainTex);

    if (PaintSpecGlossTex)
      paintableTextures[TexturePainter.MaterialType._SpecGlossMap] = CreateTexturePainter(ref m, TexturePainter.MaterialType._SpecGlossMap);

    if (PaintParallaxTex)
      paintableTextures[TexturePainter.MaterialType._ParallaxMap] = CreateTexturePainter(ref m, TexturePainter.MaterialType._ParallaxMap);
    GetComponent<Renderer>().material = m;
  }

  public void StopPaint() {
    if (IsBeingPainted) {
      foreach (var tex in paintableTextures) {
        //tex.Value.Bake(false);
        tex.Value.FinishPaint();
        //Destroy(tex.Value.gameObject);
      }
      paintableTextures.Clear();
      isBeingPainted = false;
    }
  }

  void Awake() {
    foreach(var t in TexturePainter.AllMaterialTypes) {
      orginalTextures[t] = GetComponent<Renderer>().material.GetTexture(t.ToString());
    }
  }

  public void Clean() {
    foreach (var p in particles)
      Destroy(p.gameObject);
    particles.Clear();

    if (IsBeingPainted)
      foreach (var tex in paintableTextures)
        tex.Value.Bake(true);
  }

  public TexturePainter GetTexturePainter(TexturePainter.MaterialType materialType) {
    if (!paintableTextures.ContainsKey(materialType))
      return null;
    return paintableTextures[materialType];
  }

  void OnEnable() {
    if (!isBeingPainted)
      StartPaint();
  }

  void OnDisable() {
    StopPaint();
  }

  void OnDestroy() {
    StopPaint();
  }

  private List<ParticlePaint> particles = new List<ParticlePaint>();

  public ParticlePaint addParticlePainter(ParticlePaint paint) {
    ParticlePaint p = Instantiate(paint, transform.position, transform.rotation) as ParticlePaint;
    p.transform.parent = transform;
    particles.Add(p);
    return p;
  }
}