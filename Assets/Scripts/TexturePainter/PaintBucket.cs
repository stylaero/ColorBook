﻿using UnityEngine;
using System.Collections;

public class PaintBucket : MonoBehaviour {
  public Color BucketColor;
  public float MixSpeed;

  public Color Mix(Color brushColor, Color bucketColor, float time) {
    return Color.Lerp(brushColor, bucketColor, time * MixSpeed);
  }

  public void InteractHit(Interactor.InteractionHitData data) {
      PaintBrushColor brush = data.InteractorRigidBody.GetComponentInChildren<PaintBrushColor>();
      if (brush) {
        float d = Mathf.Clamp(data.InteractorRigidBody.GetPointVelocity(data.hit.point).magnitude, 0, 2);
        brush.BrushColor = brush.Mix(brush.BrushColor, BucketColor, data.deltaTime * d);
      }
  }
}