﻿using UnityEngine;
using System.Collections;

public class PaintDrop : MonoBehaviour {
  public float radius;

  public float specularAmount;

  public GameObject PaintPrefab;
  public GameObject SpecularPaintPrefab;
  public Color CurrentColor;

  public float Lifetime;

  void Start() {
    Invoke("Kill", Lifetime);
  }

  public void Kill() {
    this.Recycle();
    //Destroy(gameObject);
  }

  public PaintableMesh PaintBrushTarget(Ray ray, float maxLength, out RaycastHit hit) {
    if (Physics.Raycast(ray, out hit, maxLength)) {
      PaintableMesh paint = hit.transform.GetComponent<PaintableMesh>();
      return paint;
    } else {
      return null;
    }
  }

  public void SetColor(Color c) {
    CurrentColor = c;
    GetComponent<Renderer>().material.color = c;
  }

  void OnCollisionEnter(Collision collision) {
    Vector3 point = collision.contacts[0].point;
    Vector3 normal = -collision.contacts[0].normal;

    Ray ray = new Ray(point - 0.5f * normal * radius, normal);
    RaycastHit hit;
    PaintableMesh paintableMesh = PaintBrushTarget(ray, radius, out hit);
    if (paintableMesh != null) {
      Vector2 uvHitPosition = hit.textureCoord;
      Quaternion rotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));

      // Color
      TexturePainter painter = paintableMesh.GetTexturePainter(TexturePainter.MaterialType._MainTex);
      GameObject paint = Instantiate(PaintPrefab,
        painter.UVPositionToRenderTexturePosition(uvHitPosition),
        rotation) as GameObject;
      paint.GetComponent<SpriteRenderer>().color = CurrentColor;
      painter.AddStroke(paint);

      // Specular texture
      /*painter = paintableMesh.GetTexturePainter(TexturePainter.MaterialType._SpecGlossMap);
      paint = Instantiate(SpecularPaintPrefab,
        painter.UVPositionToRenderTexturePosition(uvHitPosition),
        rotation) as GameObject;
      Color c = CurrentColor;
      c.a = specularAmount;
      paint.GetComponent<SpriteRenderer>().color = c;
      painter.AddStroke(paint);*/
    }
    Kill();
  }
}