﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Cursor3D : MonoBehaviour {
  public Image ColorCircle;

  public void SetPosition(Vector3 pos, Vector3 normal) {
    transform.position = pos + 0.01f*normal;
    transform.rotation = Quaternion.LookRotation(normal, transform.up);
  }

  public void SetPenPosition(Vector3 penPosition) {
    float distance = (transform.position - penPosition).magnitude;
    float scale = Mathf.Clamp(distance, 0, 2);
    scale *= 0.25f;
    scale += 0.2f;

    ColorCircle.transform.localScale = Vector3.one * scale;
  }

  public void SetColor(Color c) {
    ColorCircle.color = c;
  }

  public void SetEnabled(bool e) {
    gameObject.SetActive(e);
  }
}