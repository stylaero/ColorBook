﻿using UnityEngine;
using UnityEngine.UI;

using Stylaero.Wand3D;

public class Wand3DTrackedObject : MonoBehaviour {
  public Wand3D Wand;
  public Text text;
  public InputField wandPort;

  private Rigidbody rigidBody;

  private GameSettings settings;

  public float Scaling;

  private bool isCalibrating;

  private Vector3 rootPosition;

  public void Start() {
    settings = FindObjectOfType<GameSettings>();
    settings.OnSettingsChanged += (GameSettings.Settings currentSettings) => {
      Wand.setStandardDeviations(currentSettings.wand3dAcceleration, currentSettings.wand3dOrientation, currentSettings.wand3dGain);
      Wand.setOutlinerRejectionFactor(currentSettings.outlinerFactor);
      rootPosition = new Vector3(0, currentSettings.sensorPositionZ, 0);
      Wand.StopWand3DDevice();
      Wand.setInput(currentSettings.comPort);
      Wand.StartWand3dDevice();
    };

    rootPosition = new Vector3(0, settings.getCurrentSettings().sensorPositionZ, 0);

    rigidBody = GetComponent<Rigidbody>();
    Connect();
  }

  public void Connect() {
    DestroyWand();
    Wand = new Wand3D(wandPort.text);
    bool s = Wand.StartWand3dDevice();
    string i = Wand.GetInput();

    Wand.setStandardDeviations(settings.getCurrentSettings().wand3dAcceleration, settings.getCurrentSettings().wand3dOrientation, settings.getCurrentSettings().wand3dGain);
    Wand.setOutlinerRejectionFactor(settings.getCurrentSettings().outlinerFactor);
  }

  public void DestroyWand() {
    if (Wand != null) {
      Wand.Dispose();
      Wand = null;
    }
  }

  public void Calibrate() {
    if (Wand != null) {
      Wand.ReCalibrate();
      EventSystemScript.Instance.CalibrationStart();
      isCalibrating = true;
    }
  }


  void FixedUpdate() {
    if (Wand != null) {
      if (text.IsActive()) {
        string s = "Unity 3D Wrapper v" + Wand3D.WrapperVersion + "\n" +
                   "Input Device: " + Wand.GetInput() + "\n" +
                   "IsInitziated: " + Wand.IsInitialized() + "\n" +
                   "IsCalibrated: " + Wand.IsCalibrated() + "\n" +
                   "IsSerialInput: " + Wand.IsSerialInput() + "\n" +
                   "Position: " + Wand.GetData().Position + "\n" +
                   "Rotation: " + Wand.GetData().Rotation + "\n" +
                   "Orientation: " + Wand.GetData().Orientation + "\n" +
                   "Strength: " + Wand.GetData().Orientation.magnitude + "\n" +
                   "Velocity: " + Wand.GetData().Velocity + "\n";
        text.text = s;
      }

      if (Wand.IsCalibrated()) {
        var wandData = Wand.GetData();
        rigidBody.MovePosition(rootPosition + wandData.Position * Scaling);
        rigidBody.MoveRotation(wandData.Rotation);
        rigidBody.velocity = wandData.Velocity * Scaling;

        if (isCalibrating) {
          isCalibrating = false;
          EventSystemScript.Instance.CalibrationEnd();
        }
      }
    }
  }

  void OnDestroy() {
    DestroyWand();
  }
}