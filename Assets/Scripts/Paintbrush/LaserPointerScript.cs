﻿using UnityEngine;
using System.Collections;

public class LaserPointerScript : MonoBehaviour {
  public Cursor3D cursorPrefab;
  private Cursor3D cursor;

  public float maxRange;
  public DottedLine laserRay;

	void Awake() {
    cursor = Instantiate(cursorPrefab);
	}

  void Start() {
    GameSettings settings = FindObjectOfType<GameSettings>();
    gameObject.SetActive(settings.getCurrentSettings().laserPointerVisible);
    settings.OnSettingsChanged += (GameSettings.Settings currentSettings) => {
      gameObject.SetActive(currentSettings.laserPointerVisible);
    };
  }

	void Update() {
    Ray ray = new Ray(transform.position, -transform.up);
    RaycastHit hit;

    if (Physics.Raycast(ray, out hit, maxRange)) {
      cursor.SetEnabled(true);
      cursor.SetPosition(hit.point, hit.normal);
      cursor.SetPenPosition(transform.position);
      laserRay.SetRayPos(1, hit.point);
    } else {
      cursor.SetEnabled(false);
      laserRay.SetRayPos(1, transform.position - transform.up * maxRange);
    }
    laserRay.SetRayPos(0, transform.position);
	}

  void OnEnable() {
    if (cursor)
      cursor.SetEnabled(true);
  }

  void OnDisable() {
    if (cursor)
      cursor.SetEnabled(false);
  }

  void OnDestroy() {
    Destroy(cursor);
    cursor = null;
  }
}
