﻿using UnityEngine;
using System.Collections;

public class Interactor : MonoBehaviour {

  public class InteractionHitData {
    public RaycastHit hit;
    public Interactor interactor;
    public float deltaTime;
    public Rigidbody InteractorRigidBody;
    public InteractionHitData(RaycastHit hit, Interactor interactor, float deltaTime, Rigidbody rigidbody) {
      this.hit = hit;
      this.interactor = interactor;
      this.deltaTime = deltaTime;
      InteractorRigidBody = rigidbody;
    }
  }

  public float interactionRadius;

  public Transform interactorTip;

  public Rigidbody rigidBody;

  void FixedUpdate() {
    Vector3 interactV = -transform.position + interactorTip.position;

    Debug.DrawRay(transform.position, interactV, Color.yellow);
    Ray ray = new Ray(transform.position, interactV.normalized);
    RaycastHit hitInfo;

    if (Physics.SphereCast(ray, interactionRadius, out hitInfo, interactV.magnitude)) {
      hitInfo.transform.SendMessage("InteractHit", new InteractionHitData(hitInfo, this, Time.fixedDeltaTime, rigidBody), SendMessageOptions.DontRequireReceiver);
    }
  }

  // Use this for initialization
  void Start() {

  }
}
