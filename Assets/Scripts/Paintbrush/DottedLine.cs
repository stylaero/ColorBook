﻿using UnityEngine;
using System.Collections;

public class DottedLine : MonoBehaviour {

  private LineRenderer laserRay;
  private Vector3[] laserRayPos = new Vector3[2];
  public float textureScale;
  private Material laserRayMaterial;

  public void SetRayPos(int i, Vector3 p) {
    laserRayPos[i] = p;
    laserRay.SetPosition(i, p);
    laserRayMaterial.mainTextureScale = new Vector2(RayLength() * textureScale, 1);

    if (i == 1)
      transform.position = p;
  }

  public void SetNormal(Vector3 normal) {
    transform.rotation = Quaternion.LookRotation(normal, transform.up);
    Debug.DrawRay(transform.position, normal);
  }

  public float RayLength() {
    return (laserRayPos[0] - laserRayPos[1]).magnitude;
  }

	void Awake () {
    laserRay = GetComponent<LineRenderer>();
    laserRayMaterial = laserRay.material;
	}
}
