﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HelperLines : MonoBehaviour {
  public float radius;
  public int sphereCasts;

  public DottedLine DottedLinePrefab;
  public int MaxDottedLines;
  private DottedLine[] lines;

  public LayerMask CollideMask;

	void Start () {
    lines = new DottedLine[MaxDottedLines];
    for (int i = 0; i < MaxDottedLines; i++) {
      lines[i] = Instantiate(DottedLinePrefab);
      lines[i].gameObject.SetActive(false);
      lines[i].transform.parent = transform;
    }
    GameSettings settings = FindObjectOfType<GameSettings>();
    gameObject.SetActive(settings.getCurrentSettings().helpLinesVisible);
    settings.OnSettingsChanged += (GameSettings.Settings currentSettings) => {
      gameObject.SetActive(currentSettings.helpLinesVisible);
    };
	}
	
	void Update () {
    Ray ray = new Ray(transform.position, transform.forward);
    List<RaycastHit> allHits = new List<RaycastHit>();
    for (int i = 0; i < sphereCasts; i++) {
      RaycastHit[] hits = Physics.SphereCastAll(ray, radius/(float)i, radius/(float)i, CollideMask.value);
      foreach (var h in hits) {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.position + h.point, out hit, CollideMask.value))
          allHits.Add(hit);
      }
    }
    DrawLines(allHits);
  }

  public void DrawLines(List<RaycastHit> hits) {
    int maxLines = Mathf.Min(hits.Count, MaxDottedLines);
    for (int i = 0; i < maxLines; i++) {
      lines[i].gameObject.SetActive(true);
      lines[i].SetRayPos(0, transform.position);
      lines[i].SetRayPos(1, hits[i].point);
      lines[i].SetNormal(hits[i].normal);
    }
    for (int i = maxLines; i < lines.Length; i++)
      lines[i].gameObject.SetActive(false);
  }
}
