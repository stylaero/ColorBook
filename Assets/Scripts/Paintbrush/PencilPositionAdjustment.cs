﻿using UnityEngine;
using System.Collections;

public class PencilPositionAdjustment : MonoBehaviour {

  public GameObject pencilModel;

  public float scaleFactor;

  void Start() {
    GameSettings settings = FindObjectOfType<GameSettings>();
    settings.OnSettingsChanged += (GameSettings.Settings currentSettings) => {
      SetMagnetPosition(currentSettings.pencilMagnetPosition);
      SetMagnetRotation(currentSettings.pencilMagnetRotation);
    };
    SetMagnetPosition(settings.getCurrentSettings().pencilMagnetPosition);
    SetPencilLength(settings.getCurrentSettings().pencilLength);
  }

  private void SetMagnetPosition(float z) {
    Vector3 localPos = transform.localPosition;
    localPos.z = z;
    transform.localPosition = localPos;
  }

  private void SetMagnetRotation(float r) {
    Vector3 euler = transform.localEulerAngles;
    euler.x = r;
    transform.localEulerAngles = euler;
  }

  public void SetPencilLength(float l) {
    pencilModel.transform.localScale = Vector3.one * scaleFactor * l;
  }
}
