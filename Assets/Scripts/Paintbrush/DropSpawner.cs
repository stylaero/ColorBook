﻿using UnityEngine;
using System.Collections;

public class DropSpawner : MonoBehaviour {
  public PaintBrushColor brushColor;

  public PaintDrop DropPerfab;
  public Rigidbody pencilBody;

  private Vector3 oldVelocity;
  private Vector3 acceleration;
  private Vector3 oldAcceleration;
  private Vector3 jerk;

  public float accelerationTreshold;

	void FixedUpdate () {
    Vector3 vel = pencilBody.GetPointVelocity(transform.position);
    acceleration = (vel - oldVelocity) * (1 / Time.fixedDeltaTime);
    jerk = (acceleration - oldAcceleration) * (1 / Time.fixedDeltaTime);
    if (jerk.magnitude > accelerationTreshold && brushColor.WaterAmount > 0 ) {
      PaintDrop paint = DropPerfab.Spawn(transform.position, Quaternion.identity);
      paint.SetColor(brushColor.BrushColor);
      paint.GetComponent<Rigidbody>().velocity = vel;
      brushColor.WaterAmount -= Time.fixedDeltaTime;
    }
    oldVelocity = vel;
    oldAcceleration = acceleration;
	}
}
