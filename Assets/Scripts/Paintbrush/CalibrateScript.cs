﻿using UnityEngine;

public class CalibrateScript : MonoBehaviour {
  public GameObject broomModel;
  public GameObject wand3dRoot;
  public GameObject calibrateGoal;

  public GUIButton button;

  void Awake() {
    button.OnButtonPressed += Calibrated;
  }

  public void Calibrated() {
    gameObject.SetActive(false);
    GameSettings settings = FindObjectOfType<GameSettings>();
    Vector3 rotation = broomModel.transform.localEulerAngles;
    settings.setPencilMagnetPosition(broomModel.transform.localPosition.z);
    settings.setPencilMagnetRotation(rotation.x);
  }

  void OnEnable() {
    button.Reset();
  }

	void FixedUpdate () {
    bool flip = wand3dRoot.transform.eulerAngles.x < 0 || wand3dRoot.transform.eulerAngles.x > 90;
    if (flip) {
      broomModel.transform.localEulerAngles = new Vector3(90, 0, 0);
    } else {
      broomModel.transform.localEulerAngles = new Vector3(-90, 0, 0);
    }

    Vector3 localPos = broomModel.transform.localPosition;
    broomModel.transform.position = calibrateGoal.transform.position;
    float z = broomModel.transform.localPosition.z;
    localPos.z = z;
    broomModel.transform.localPosition = localPos;
	}
}