﻿using UnityEngine;
using System.Collections;

public class BrushVisualEffect : MonoBehaviour {

  private Material[] materials;
  public int brushMaterialIndex;

  public PaintBrushColor brush;

  public void SetColor(Color c) {
    foreach (Material m in materials) {
      m.SetColor("_RimColor", c);
    }
    materials[brushMaterialIndex].SetColor("_Color", c);
  }

  public void SetWater(float w) {

  }

	void Start () {
    materials = GetComponent<Renderer>().materials;
    brush.OnColorChanged += SetColor;
    brush.OnWaterChanged += SetWater;
    SetColor(brush.BrushColor);
    SetWater(brush.WaterAmount);
	}
}
