﻿using UnityEngine;
using Stylaero.Wand3D;

/*
 * Options Menu system
 */

public class GameSettings : MonoBehaviour {

  public class Settings {
    public float musicVolume;
    public float effectVolume;

    public float wand3dAcceleration = 14;
    public float wand3dOrientation = 14;
    public float wand3dGain = 0.000000006f;

    public float sensorPositionZ;
    public float pencilMagnetPosition = 1.9f;
    public float pencilLength = 1;
    public float pencilMagnetRotation = 270;

    public double outlinerFactor = 25 * 25;

    public bool helpLinesVisible = false;
    public bool laserPointerVisible = false;

    public string comPort;

    public bool fullscreen {
      get {
        return Screen.fullScreen;
      }
    }

    public int width {
      get {
        return Screen.width;
      }
    }

    public int height {
      get {
        return Screen.height;
      }
    }

    public int refreshRate {
      get {
        return Screen.currentResolution.refreshRate;
      }
    }

    public int QualityLevel {
      get {
        return QualitySettings.GetQualityLevel();
      }
    }

    public Settings() {
      if (PlayerPrefs.HasKey("musicVolume")) musicVolume = PlayerPrefs.GetFloat("musicVolume");
      if (PlayerPrefs.HasKey("effectVolume")) effectVolume = PlayerPrefs.GetFloat("effectVolume");

      if (PlayerPrefs.HasKey("wand3dAcceleration")) wand3dAcceleration = PlayerPrefs.GetFloat("wand3dAcceleration");
      if (PlayerPrefs.HasKey("wand3dOrientation")) wand3dOrientation = PlayerPrefs.GetFloat("wand3dOrientation");
      if (PlayerPrefs.HasKey("wand3dGain")) wand3dGain = PlayerPrefs.GetFloat("wand3dGain");

      if (PlayerPrefs.HasKey("wand3dPosZ")) sensorPositionZ = PlayerPrefs.GetFloat("wand3dPosZ");
      if (PlayerPrefs.HasKey("wand3dMagnetPosZ")) pencilMagnetPosition = PlayerPrefs.GetFloat("wand3dMagnetPosZ");
      if (PlayerPrefs.HasKey("wand3dMagnetRotation")) pencilMagnetRotation = PlayerPrefs.GetFloat("wand3dMagnetRotation");
      if (PlayerPrefs.HasKey("wand3dPencilLength")) pencilLength = PlayerPrefs.GetFloat("wand3dPencilLength");

      if (PlayerPrefs.HasKey("laserPointerVisible")) laserPointerVisible = PlayerPrefs.GetInt("laserPointerVisible") != 0;
      if (PlayerPrefs.HasKey("helpLinesVisible")) helpLinesVisible = PlayerPrefs.GetInt("helpLinesVisible") != 0;
      if (PlayerPrefs.HasKey("wand3dOutlinerFactor")) outlinerFactor = PlayerPrefs.GetFloat("wand3dOutlinerFactor");

      comPort = Wand3D.DefaultPort;
      if (PlayerPrefs.HasKey("comPort")) comPort = PlayerPrefs.GetString("comPort");
    }

    public void SaveSettings() {
      PlayerPrefs.SetFloat("musicVolume", musicVolume);
      PlayerPrefs.SetFloat("effectVolume", effectVolume);
      PlayerPrefs.SetString("comPort", comPort);
      PlayerPrefs.SetFloat("wand3dAcceleration", wand3dAcceleration);
      PlayerPrefs.SetFloat("wand3dOrientation", wand3dOrientation);
      PlayerPrefs.SetFloat("wand3dGain", wand3dGain);
      PlayerPrefs.SetFloat("wand3dPosZ", sensorPositionZ);
      PlayerPrefs.SetFloat("wand3dMagnetPosZ", pencilMagnetPosition);
      PlayerPrefs.SetFloat("wand3dPencilLength", pencilLength);
      PlayerPrefs.SetFloat("wand3dMagnetRotation", pencilMagnetRotation);
      PlayerPrefs.SetFloat("wand3dOutlinerFactor", (float)outlinerFactor);

      PlayerPrefs.SetInt("laserPointerVisible", (laserPointerVisible) ? 1 : 0);
      PlayerPrefs.SetInt("helpLinesVisible", (helpLinesVisible) ? 1 : 0);
    }
  }

  public delegate void SettingsChanged(Settings settings);
  public event SettingsChanged OnSettingsChanged;

  private Settings currentSettings;

  private void notifySettingsChanged() {
    currentSettings.SaveSettings();
    if (OnSettingsChanged != null)
      OnSettingsChanged(currentSettings);
  }

  public Settings LoadSettings() {
    return new Settings();
  }

  public Settings getCurrentSettings() {
    if (currentSettings == null) {
      currentSettings = LoadSettings();
      QualitySettings.SetQualityLevel(currentSettings.QualityLevel, true);
    }
    return currentSettings;
  }

  public void setResolution(int width, int height, int refreshRate, bool fullscreen) {
    Screen.SetResolution(width, height, fullscreen, refreshRate);
    notifySettingsChanged();
  }

  public void setResolution(int width, int height, int refreshRate) {
    Settings settings = getCurrentSettings();
    setResolution(width, height, refreshRate, settings.fullscreen);
  }

  public void setFullscreen(bool fullscreen) {
    Settings settings = getCurrentSettings();
    setResolution(settings.width, settings.height, settings.refreshRate, fullscreen);
  }

  public void setMusicVolume(float musicLevel) {
    getCurrentSettings().musicVolume = musicLevel;
    notifySettingsChanged();
  }

  public void setEffectVolume(float level) {
    getCurrentSettings().effectVolume = level;
    notifySettingsChanged();
  }

  public void setQualityLevel(int level) {
    QualitySettings.SetQualityLevel(level, true);
    notifySettingsChanged();
  }

  public void setTrackingParameters(float acceleration, float orientation, float gain) {
    getCurrentSettings().wand3dAcceleration = acceleration;
    getCurrentSettings().wand3dOrientation = orientation;
    getCurrentSettings().wand3dGain = gain;
    notifySettingsChanged();
  }

  public void setComPort(string port) {
    getCurrentSettings().comPort = port;
    notifySettingsChanged();
  }

  public void setSensorPositionZ(float z) {
    getCurrentSettings().sensorPositionZ = z;
    notifySettingsChanged();
  }

  public void setPencilMagnetPosition(float p) {
    getCurrentSettings().pencilMagnetPosition = p;
    notifySettingsChanged();
  }

  public void setPencilLength(float l) {
    getCurrentSettings().pencilLength = l;
    notifySettingsChanged();
  }

  public void setLaserPointerVisible(bool visible) {
    getCurrentSettings().laserPointerVisible = visible;
    notifySettingsChanged();
  }

  public void setHelpLinesVisible(bool visible) {
    getCurrentSettings().helpLinesVisible = visible;
    notifySettingsChanged();
  }

  public void setOutlinerFactor(double outlinerFactor) {
    getCurrentSettings().outlinerFactor = outlinerFactor;
    notifySettingsChanged();
  }

  public void setPencilMagnetRotation(float rotation) {
    getCurrentSettings().pencilMagnetRotation = rotation;
    notifySettingsChanged();
  }
}