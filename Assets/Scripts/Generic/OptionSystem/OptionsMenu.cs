﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Stylaero.Wand3D;

/*
 * Options Menu system
 */
public class OptionsMenu : MonoBehaviour {
  [Header("UI option elements")]
  public Dropdown qualitySetting;
  public Slider effectVolume;
  public Dropdown resolutionSetting;
  public Toggle isFullscreen;
  public InputField comPort;

  public Slider AccelerationSlider;
  public Slider OrientationSlider;
  public Slider GainSlider;

  public InputField sensorPositionZ;
  public InputField outlinerFactorField;

  public Toggle showLaserPointer;
  public Toggle showHelpLines;

  [Header("UI text elements")]
  public Text accelerationText;
  public Text orientationText;
  public Text gainText;

  private GameSettings settings;
  private Resolution[] resolutions;

  void Awake() {
    settings = FindObjectOfType<GameSettings>();
    RebuildGUI(settings.getCurrentSettings());

    qualitySetting.onValueChanged.AddListener(SetQualityLevel);
    resolutionSetting.onValueChanged.AddListener(SetResolution);
    isFullscreen.onValueChanged.AddListener(SetFullscreen);

    AccelerationSlider.onValueChanged.AddListener(SetTrackingParameters);
    OrientationSlider.onValueChanged.AddListener(SetTrackingParameters);
    GainSlider.onValueChanged.AddListener(SetTrackingParameters);

    comPort.onEndEdit.AddListener(SetComPort);
    effectVolume.onValueChanged.AddListener(SetEffectVolume);
    sensorPositionZ.onValueChanged.AddListener(SetSensorPositionZ);
    showLaserPointer.onValueChanged.AddListener((bool value) => { settings.setLaserPointerVisible(value); });
    showHelpLines.onValueChanged.AddListener((bool value) => { settings.setHelpLinesVisible(value); });

    outlinerFactorField.onEndEdit.AddListener(SetOutlinerFactor);
  }


  public void RebuildGUI(GameSettings.Settings settings) {
    BuildQualitySettings();
    qualitySetting.value = settings.QualityLevel;

    effectVolume.value = settings.effectVolume;
    isFullscreen.isOn = settings.fullscreen;
    comPort.text = settings.comPort;

    AccelerationSlider.value = Mathf.Log(settings.wand3dAcceleration, 10);
    OrientationSlider.value = Mathf.Log(settings.wand3dOrientation, 10);
    GainSlider.value = Mathf.Log(settings.wand3dGain, 10);

    accelerationText.text = string.Format("Acceleration: {0:E2}", settings.wand3dAcceleration);
    orientationText.text = string.Format("Orientation: {0:E2}", settings.wand3dOrientation);
    gainText.text = string.Format("Gain: {0:E2}", settings.wand3dGain);

    sensorPositionZ.text = string.Format("{0}", settings.sensorPositionZ);

    showLaserPointer.isOn = settings.laserPointerVisible;
    showHelpLines.isOn = settings.helpLinesVisible;

    outlinerFactorField.text = settings.outlinerFactor.ToString();

    BuildResolutionSettings();
    resolutionSetting.value = getSelectedResolution(settings);
  }


  private string getResolutionName(Resolution r) {
    return "" + r.width + " x " + r.height + " " + r.refreshRate + " Hz";
  }


  public void BuildResolutionSettings() {
    resolutions = Screen.resolutions;

    List<Dropdown.OptionData> resolutionsOptions = new List<Dropdown.OptionData>();
    foreach (Resolution res in resolutions) {
      resolutionsOptions.Add(new Dropdown.OptionData(getResolutionName(res)));
    }
    resolutionSetting.options = resolutionsOptions;
  }

  public void BuildQualitySettings() {
    List<Dropdown.OptionData> qualityOptions = new List<Dropdown.OptionData>();
    foreach (var q in QualitySettings.names)
      qualityOptions.Add(new Dropdown.OptionData(q));
    qualitySetting.options = qualityOptions;
  }

  private int getSelectedResolution(GameSettings.Settings settings) {
    for (int i = 0; i < resolutions.Length; i++) {
      Resolution r = resolutions[i];
      if (r.width == settings.width && r.height == settings.height && r.refreshRate == settings.refreshRate)
        return i;
    }
    return 0;
  }

  public void SetFullscreen(bool f) {
    settings.setFullscreen(f);
  }

  public void SetTrackingParameters(float f) {
    settings.setTrackingParameters(Mathf.Pow(10, AccelerationSlider.value),
      Mathf.Pow(10, OrientationSlider.value),
      Mathf.Pow(10, GainSlider.value));
    accelerationText.text = string.Format("Acceleration: {0:E2}", settings.getCurrentSettings().wand3dAcceleration);
    orientationText.text = string.Format("Orientation: {0:E2}", settings.getCurrentSettings().wand3dOrientation);
    gainText.text = string.Format("Gain: {0:E2}", settings.getCurrentSettings().wand3dGain);
  }

  public void SetEffectVolume(float f) {
    settings.setEffectVolume(effectVolume.value);
  }

  public void SetQualityLevel(int level) {
    settings.setQualityLevel(level);
  }

  public void SetComPort(string port) {
    settings.setComPort(port);
  }

  public void SetResolution(int level) {
    Resolution selected = resolutions[level];
    settings.setResolution(selected.width, selected.height, selected.refreshRate);
  }

  public void SetOutlinerFactor(string value) {
    double factor;
    if (double.TryParse(value, out factor))
      settings.setOutlinerFactor(factor);
    else
      outlinerFactorField.text = settings.getCurrentSettings().outlinerFactor.ToString();
  }

  public void SetSensorPositionZ(string value) {
    float factor;
    if (float.TryParse(value, out factor))
      settings.setSensorPositionZ(factor);
    else
      sensorPositionZ.text = settings.getCurrentSettings().sensorPositionZ.ToString();
  }

  public void ResetSettings() {
    AccelerationSlider.value = 2;
    OrientationSlider.value = 2;
    GainSlider.value = -6;
    sensorPositionZ.text = "0";
    outlinerFactorField.text = "625";
    comPort.text = Wand3D.DefaultPort;
  }
}