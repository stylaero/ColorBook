﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ModelSelector : MonoBehaviour {
  public GameObject[] models;

  private int active;
  private Dropdown dropdown;

	// Use this for initialization
	void Start () {
    dropdown = GetComponent<Dropdown>();
    CreateDropdown();
	}

  public void CreateDropdown() {
    List<Dropdown.OptionData> data = new List<Dropdown.OptionData>();
    foreach (var v in models) {
      data.Add(new Dropdown.OptionData(v.name));
    }
    dropdown.options = data;
  }

  public void SetActive(int i) {
    models[active].SetActive(false);
    models[i].SetActive(true);
    active = i;
  }
}
