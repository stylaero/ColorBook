﻿using UnityEngine;
using System.Collections;

public class CalibrationMessage : MonoBehaviour {
	void Start () {
    EventSystemScript.Instance.OnCalibrationEnd += Hide;
    EventSystemScript.Instance.OnCalibrationStart += Show;
    Hide();
	}

  public void Show() {
    gameObject.SetActive(true);
  }

  public void Hide() {
    gameObject.SetActive(false);
  }
}