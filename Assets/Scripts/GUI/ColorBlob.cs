﻿using UnityEngine;
using UnityEngine.UI;

public class ColorBlob : MonoBehaviour {
  public PaintBrushColor brush;
  private Image image;

	void Start () {
    image = GetComponent<Image>();
	}
	
	void Update () {
    Color c = brush.BrushColor;
    c.a = 1;
    image.color = c;
	}
}
