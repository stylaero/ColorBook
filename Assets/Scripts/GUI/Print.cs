﻿using System.IO;
using UnityEngine;
using System.Collections;

public class Print : MonoBehaviour {
  private int ScreenshotCounter = 0;
  private string filepath;
  private string filename;
  

  public string GetScreenshotName(int i) {
    return Path.Combine(Application.dataPath, "Screenshot" + i.ToString("D4") + ".png");
  }

  public void TakeScreenshot() {
    EventSystemScript.Instance.TakeScreenshotStart();

    filepath = GetScreenshotName(ScreenshotCounter);
    filename = "" + ScreenshotCounter;
    while (System.IO.File.Exists(filepath)) {
      ScreenshotCounter++;
      filepath = GetScreenshotName(ScreenshotCounter);
      filename = "" + ScreenshotCounter;
    }
    Application.CaptureScreenshot(filepath);
    ScreenshotCounter++;
    Invoke("ScreenshotFinished", 0.1f);
  }

  public void ScreenshotFinished() {
    EventSystemScript.Instance.TakeScreenshotEnd(filename, filepath);
  }
}
