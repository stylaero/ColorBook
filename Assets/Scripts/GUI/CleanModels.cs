﻿using UnityEngine;
using System.Collections;

public class CleanModels : MonoBehaviour {

  public void CleanAll() {
    foreach (var v in FindObjectsOfType<PaintableMesh>())
      v.Clean();
  }
}
