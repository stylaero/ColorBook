﻿using UnityEngine;
using System.Collections;

public class ToggleGUI : MonoBehaviour {
  public GameObject[] GUI;
  private bool visible = true;

  void Update() {
    if (Input.GetButtonDown("ToggleGUI")) {
      SetGUIVisible(!visible);
    }
  }

  public void SetGUIVisible(bool visible) {
    foreach (var g in GUI)
      g.SetActive(visible);
    this.visible = visible;
  }

}