﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenshotMessage : MonoBehaviour {
  public Text text;
  public float displayTime;

	void Start () {
    EventSystemScript.Instance.OnScreenshotEnd += ScreenshotTaken;
    EventSystemScript.Instance.OnScreenshotStart += Hide;
    Hide();
	}

  public void ScreenshotTaken(string name, string path) {
    CancelInvoke();
    text.text = name;
    Show();
    Invoke("Hide", displayTime);
  }

  public void Show() {
    gameObject.SetActive(true);
  }
  public void Hide() {
    gameObject.SetActive(false);
  }
}
