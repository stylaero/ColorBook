﻿using UnityEngine;

public class SoundOnTouch : MonoBehaviour {

  public AudioSource touchLoop;
  public AudioSource contactSound;
  public AudioSource releaseSound;

  public float minContactSpeed;
  public float contactLerpSpeed;
  private float contact;
  private bool hasContact;

  public void Impact() {
    if (!hasContact) {
      hasContact = true;
      if (contactSound)
        contactSound.Play();
    }
  }

  public void Contact(Vector3 v, float speed, float time) {
    if (speed > minContactSpeed)
      contact = 1;
    touchLoop.transform.position = v;
  }

  public void Release() {
    if (hasContact) {
      hasContact = false;
      if (releaseSound)
        releaseSound.Play();
    }
  }
	
	void Update () {
    if (touchLoop)
      touchLoop.volume = contact;
    contact = Mathf.Lerp(contact, 0, contactLerpSpeed * Time.deltaTime);
	}
}