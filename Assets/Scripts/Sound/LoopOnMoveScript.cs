﻿using UnityEngine;

public class LoopOnMoveScript : MonoBehaviour {
  public AudioSource loop;
  public AudioSource contact;
  public ModelBrush modelBrush;

  public float pitchMin;
  public float pitchMax;
  public float pitchScale;

  public float brushSpeedMin;
  private bool playingPaintAudio;

  void Start() {
    modelBrush.OnBrushImpact += ImpactSound;
  }

  public void ImpactSound(Vector3 v) {
      contact.Play();
  }

	void Update () {
    bool paintSound = modelBrush.IsPainting && modelBrush.BrushSpeed > brushSpeedMin;

    if (paintSound && !playingPaintAudio) {
      loop.volume = 1;
      playingPaintAudio = true;
    } else if (!paintSound && playingPaintAudio) {
      loop.volume = 0;
      playingPaintAudio = false;
    }
    //loop.pitch = Mathf.Clamp(modelBrush.BrushSpeed * pitchScale, pitchMin, pitchMax);
  }
}
