﻿using UnityEngine;
using System.Collections;

public class NextModelButton : MonoBehaviour {

  private int selectedModel = 0;

  private ModelSelector modelSelector;

	// Use this for initialization
	void Start () {
    modelSelector = FindObjectOfType<ModelSelector>();
    GetComponent<GUIButton>().OnButtonPressed += Next;
  }

  public void Next() {
    selectedModel = (selectedModel + 1) % modelSelector.models.Length;
    modelSelector.SetActive(selectedModel);
  }
}
