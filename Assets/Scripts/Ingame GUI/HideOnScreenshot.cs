﻿using UnityEngine;
using System.Collections;

public class HideOnScreenshot : MonoBehaviour {
	void Start () {
    EventSystemScript.Instance.OnScreenshotEnd += OnScreenshotEnd;
    EventSystemScript.Instance.OnScreenshotStart += OnScreenshotStart;
	}

  private bool wasActive;

  public void OnScreenshotStart() {
    wasActive = gameObject.activeInHierarchy;
    if (wasActive)
      gameObject.SetActive(false);
  }

  public void OnScreenshotEnd(string filename, string path) {
    if (wasActive)
      gameObject.SetActive(true);
  }

  void OnDestroy() {
    EventSystemScript.Instance.OnScreenshotEnd -= OnScreenshotEnd;
    EventSystemScript.Instance.OnScreenshotStart -= OnScreenshotStart;
  }
}
