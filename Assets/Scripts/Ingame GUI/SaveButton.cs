﻿using UnityEngine;
using System.Collections;

public class SaveButton : MonoBehaviour {
  private Print printFunction;

  public float hideTime;

  public void Start() {
    printFunction = FindObjectOfType<Print>();
    GetComponent<GUIButton>().OnButtonPressed += Save;
  }

  public void Save() {
    printFunction.TakeScreenshot();
    gameObject.SetActive(false);
    Invoke("Show", hideTime);
  }

  public void Show() {
    gameObject.SetActive(true);
  }
}
