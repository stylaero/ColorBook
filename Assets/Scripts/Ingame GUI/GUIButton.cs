﻿using UnityEngine;
using UnityEngine.UI;

public class GUIButton : MonoBehaviour {
  public Color ActiveColor;
  public Color ActivatedColor;
  public Color InactiveColor;

  public Image ButtonImage;
  public Image SliderImage;

  public float ActivateTime;
  public float ActivateTrigger;
  private float activeTimer;

  private bool Activated;
  private bool selected;

  public delegate void ButtonPressed();
  public event ButtonPressed OnButtonPressed;

  [Header("Sound Effects")]
  public AudioSource pressingSound;
  public AudioSource pressedSound;

  private void UpdateGraphic() {
    if (Activated)
      ButtonImage.color = ActivatedColor;
    else if (selected)
      ButtonImage.color = ActiveColor;
    else
      ButtonImage.color = InactiveColor;

    SliderImage.fillAmount = activeTimer / (float)ActivateTime;
  }

  public void InteractHit(Interactor.InteractionHitData data) {
    selected = true;
  }

	void FixedUpdate () {
    if (selected) {
      activeTimer += Time.fixedDeltaTime;
      pressingSound.volume = 1;
    } else {
      activeTimer -= Time.fixedDeltaTime;
      pressingSound.volume = 0;
    }
    activeTimer = Mathf.Clamp(activeTimer, 0, ActivateTime);

    if (activeTimer > ActivateTrigger) {
      if (!Activated && OnButtonPressed != null) {
        Activated = true;
        Reset();
        pressedSound.Play();
        OnButtonPressed();
      }
    } else {
      Activated = false;
    }

    UpdateGraphic();
    selected = false;
  }

  public void Reset() {
    activeTimer = 0;
    Activated = false;
  }

  public bool Solved() {
    return Activated;
  }
}