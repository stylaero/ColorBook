﻿using UnityEngine;
using System.Collections;

public class ClearButton : MonoBehaviour {
  public void Start() {
    GetComponent<GUIButton>().OnButtonPressed += Clear;
  }

  public void Clear() {
    foreach (var v in FindObjectsOfType<PaintableMesh>())
      v.Clean();
  }
}