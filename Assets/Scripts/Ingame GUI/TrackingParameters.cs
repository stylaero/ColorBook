﻿using UnityEngine;
using UnityEngine.UI;

public class TrackingParameters : MonoBehaviour {
  private Wand3DTrackedObject wand3d;

  [Header("UI text elements")]
  public Text accelerationText;
  public Text orientationText;
  public Text gainText;

  [Header("UI slider elements")]
  public Slider accelerationSlider;
  public Slider orientationSlider;
  public Slider gainSlider;

  private float acceleration;
  private float orientation;
  private float gain;

	void Awake () {
    wand3d = FindObjectOfType<Wand3DTrackedObject>();

    acceleration = accelerationSlider.value;
    orientation = orientationSlider.value;
    gain = gainSlider.value;
    UpdateParameters();
	}
	
  public void UpdateParameters() {
    if (wand3d.Wand!= null) {
      wand3d.Wand.setStandardDeviations(acceleration, orientation, gain);
    }
    accelerationText.text = string.Format("Acceleration: {0:E2}", acceleration);
    orientationText.text = string.Format("Orientation: {0:E2}", orientation);
    gainText.text = string.Format("Gain: {0:E2}", gain);
  }

  public void SetAcceleration(float a) {
    acceleration = Mathf.Pow(10, a);
    UpdateParameters();
  }

  public void SetOrientation(float o) {
    orientation = Mathf.Pow(10, o);
    UpdateParameters();
  }

  public void SetGain(float g) {
    gain = Mathf.Pow(10, g);
    UpdateParameters();
  }
}