﻿using UnityEngine;

public class ModelRotator : MonoBehaviour {
  private bool isRotating;
  private Vector3 oldPos;
  public float rotationSpeed;

  public float rotationGrabLerp;
  public float rotationSlowLerp;
  public float MaxRotationSpeed;

  private Vector3 startPos;
  private float angleSpeed;

  void Update() {
    rotationSpeed = Mathf.Clamp(rotationSpeed, -MaxRotationSpeed, MaxRotationSpeed);
    transform.parent.Rotate(0, rotationSpeed * Time.deltaTime, 0);
    rotationSpeed = Mathf.Lerp(rotationSpeed, 0.0f, rotationSlowLerp * Time.deltaTime);
  }

  public void InteractHit(Interactor.InteractionHitData data) {
    Vector3 middlePos = data.hit.point;
    Vector3 vel = data.InteractorRigidBody.GetPointVelocity(middlePos);
    Quaternion goal = Quaternion.LookRotation(transform.parent.position - middlePos, transform.up);
    Vector3 e = goal.eulerAngles;
    e.x = 0;
    e.z = 0;
    e.y = goal.eulerAngles.y;
    goal.eulerAngles = e;

    Quaternion newPosition = Quaternion.LookRotation(transform.parent.position - middlePos + vel, transform.up);
    e = newPosition.eulerAngles;
    e.x = 0;
    e.z = 0;
    e.y = newPosition.eulerAngles.y;
    newPosition.eulerAngles = e;
    float delta = (-newPosition.eulerAngles.y + goal.eulerAngles.y);
    rotationSpeed = Mathf.LerpAngle(rotationSpeed, delta, rotationGrabLerp * data.deltaTime);
  }
}