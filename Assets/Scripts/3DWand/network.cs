﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Stylaero.Wand3D {

  public class Network : IDisposable {
    
    private IntPtr _network;
    private bool shouldDestroy;

    [DllImport("wand3d")]
    private static extern ErrorCode createWand3dNetwork(out IntPtr device);

    [DllImport("wand3d")]
    private static extern ErrorCode destroyWand3dNetwork(IntPtr wand3dDevice);

    [DllImport("wand3d")]
    private static extern ErrorCode getSensorPosition(IntPtr device, uint sensor, double[] position);

    [DllImport("wand3d")]
    private static extern ErrorCode setSensorPosition(IntPtr device, uint sensor, double x, double y, double z);

    [DllImport("wand3d")]
    private static extern ErrorCode setSensorRotation(IntPtr device, uint sensor, double[] R);

    [DllImport("wand3d")]
    private static extern ErrorCode getSensorRotation(IntPtr device, uint sensor, double[] R);

    [DllImport("wand3d")]
    private static extern ErrorCode getTrackingVolume(IntPtr device, double[] vol);

    [DllImport("wand3d")]
    private static extern ErrorCode setTrackingVolume(IntPtr device, double[] vol);

    public Network() {
      ErrorCode success = createWand3dNetwork(out _network);
      Wand3D.CheckForError(success);
      shouldDestroy = true;
    }

    public Network(IntPtr network) {
      _network = network;
      shouldDestroy = false;
    }


    public Vector3 GetSensorPosition(int sensor) {
      double[] positions = new double[3];
      Wand3D.CheckForError(getSensorPosition(_network, checked((uint)sensor), positions));
      return new Vector3((float)positions[0], (float)positions[2], (float)positions[1]);
    }

    public void SetSensorPosition(int sensor, Vector3 position) {
      Wand3D.CheckForError(setSensorPosition(_network, checked((uint)sensor), position.x, position.z, position.y));
    }

    public void SetSensorRotation(int sensor, Quaternion r) {
      Vector3 x = r * Vector3.right;
      Vector3 z = r * Vector3.up;
      Vector3 y = r * Vector3.forward;
      double[] rot = new double[] {
        x.x, y.x, z.x,
        x.z, y.z, z.z,
        x.y, y.y, z.y
      }; 
      Wand3D.CheckForError(setSensorRotation(_network, checked((uint)sensor), rot));
    }

    public Quaternion GetSensorRotation(int sensor) {
      double[] r = new double[9];
      Wand3D.CheckForError(getSensorRotation(_network, checked((uint)sensor), r));

      Vector3 forward = new Vector3((float)r[0], (float)r[6], (float)r[3]);
      Vector3 up = new Vector3((float)r[2], (float)r[8], (float)r[5]);
      return Quaternion.LookRotation(forward, up);
    }

    public Bounds GetTrackingVolume() {
      double[] vol = new double[6];
      Wand3D.CheckForError(getTrackingVolume(_network, vol));

      Vector3 pos = new Vector3((float)(vol[1] + vol[0]) / 2f, (float)(vol[5] + vol[4]) / 2f, (float)(vol[3] + vol[2]) / 2f);
      Vector3 size = new Vector3((float)(vol[1] - vol[0]), (float)(vol[5] - vol[4]), (float)(vol[3] - vol[2]));
      return new Bounds(pos, size);
    }

    public void SetTrackingVolume(Bounds volume) {
      double[] vol = new double[6];
      //xmin, xmax, ymin, ymax, zmin, zmax

      vol[0] = volume.center.x - volume.size.x/2f;
      vol[1] = volume.center.x + volume.size.x/2f;

      vol[2] = volume.center.z - volume.size.z/2f;
      vol[3] = volume.center.z + volume.size.z/2f;

      vol[4] = volume.center.y - volume.size.y/2f;
      vol[5] = volume.center.y + volume.size.y/2f;

      Wand3D.CheckForError(setTrackingVolume(_network, vol));
    }

    #region IDisposable Support
    private bool disposedValue = false; // To detect redundant calls

    protected virtual void Dispose(bool disposing) {
      if (!disposedValue) {
        if (disposing) {
          // TODO: dispose managed state (managed objects).
        }

        // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
        // TODO: set large fields to null.
        if (shouldDestroy)
          destroyWand3dNetwork(_network);
        _network = IntPtr.Zero;

        disposedValue = true;
      }
    }

    // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
    ~Network() {
      //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
      Dispose(false);
    }

    // This code added to correctly implement the disposable pattern.
    public void Dispose() {
      // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
      Dispose(true);
      // TODO: uncomment the following line if the finalizer is overridden above.
      // GC.SuppressFinalize(this);
    }
    #endregion
  }
}
