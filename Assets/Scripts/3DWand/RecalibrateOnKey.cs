﻿using UnityEngine;
using System.Collections;

public class RecalibrateOnKey : MonoBehaviour {
  private Wand3DTrackedObject wand3d;

	void Start () {
    wand3d = GetComponent<Wand3DTrackedObject>();
	}
	
	void Update () {
    if (Input.GetButton("Recalibrate"))
      wand3d.Calibrate();
	}
}
