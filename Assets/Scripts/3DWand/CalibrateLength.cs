﻿using UnityEngine;
using System.Collections;

public class CalibrateLength : MonoBehaviour {
  public PencilPositionAdjustment pencil;
  public GameObject pencilModel;
  public GUIButton button;

  void Awake() {
    button.OnButtonPressed += Calibrated;
  }

  public void Calibrated() {
    gameObject.SetActive(false);
    GameSettings settings = FindObjectOfType<GameSettings>();
    settings.setPencilLength(getDistance());
  }

  private float getDistance() {
    return (transform.position - pencilModel.transform.position).magnitude;
  }

  void OnEnable() {
    button.Reset();
  }

  void FixedUpdate() {
    pencil.SetPencilLength(getDistance());
  }
}