﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Stylaero.Wand3D;

public class CheckSensorStatus : MonoBehaviour {
  private Wand3D wand;

  public Text uiText;

	void Start () {
    wand = FindObjectOfType<Wand3DTrackedObject>().Wand;
	}

  public string getText(int channel, bool ok) {
    return "<b>Channel " + channel + "</b>\t" + 
      ((!ok) ? "<color=red>Rejected</color>" : "<color=green>Ok</color>") + 
      "\t";
  }
	
	void Update () {
    if (!wand.IsCalibrated() ||!wand.IsInitialized())
      return;

    bool[] status = wand.GetSensorStatus();

    string s = "";

    for (int i = 0; i < status.Length;i++) {
      s += getText(i, status[i]);
      if (!status[i])
        Debug.Log("Rejecting channel " + i);
    }

    uiText.text = s;
	}
}
