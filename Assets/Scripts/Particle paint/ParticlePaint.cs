﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticlePaint : MonoBehaviour {
  private class ParticleSpawners {
    public ParticleSpawners(Vector3 hit, Vector3 normal) {
      hitPos = hit;
      this.normal = normal;
    }
    public Vector3 hitPos;
    public Vector3 normal;
  }

  private List<ParticleSpawners> particlePositions = new List<ParticleSpawners>();
  private ParticleSystem particleSystem;
  public float spawnRate;

  public float distanceFromMesh;
  public float randomFactor;
  public float minDistance;

  public string kind;

  public bool SameType(ParticlePaint other) {
    if (other == null)
      return false;
    return kind.CompareTo(other.kind) == 0;
  }

	void Start () {
    particleSystem = GetComponent<ParticleSystem>();
    SpawnParticle();
	}

  void Update() {
    foreach (var particle in particlePositions) {
      Debug.DrawRay(transform.TransformPoint(particle.hitPos), transform.TransformDirection(particle.normal));
    }
  }

  public void addPoint(RaycastHit h) {
    // Convert to local position
    var p = transform.InverseTransformPoint(h.point);
    var n = transform.InverseTransformDirection(h.normal);

    foreach (var particle in particlePositions) {
      if ((p - particle.hitPos).magnitude < minDistance)
        return;
    }
    particlePositions.Add(new ParticleSpawners(p, n));
  }


  public void SpawnParticle() {
    if (particlePositions.Count > 0) {
      ParticleSystem.EmitParams p = new ParticleSystem.EmitParams();
      ParticleSpawners randomPos = particlePositions[Random.Range(0, particlePositions.Count)];
      p.position = transform.TransformPoint(randomPos.hitPos) + transform.TransformDirection(randomPos.normal).normalized * distanceFromMesh + Random.insideUnitSphere * randomFactor;
      particleSystem.Emit(p, 1);
    }
    Invoke("SpawnParticle", (1.0f / spawnRate) / Mathf.Max(particlePositions.Count, 1));
  }
}
