﻿using UnityEngine;
using System.Collections;

public class PartcleBucket : MonoBehaviour {
  public ParticlePaint paintParticles;
  public GameObject paintParticlesBrush;

  public void InteractHit(Interactor.InteractionHitData data) {
    PaintBrushParticles brush = data.InteractorRigidBody.GetComponentInChildren<PaintBrushParticles>();
    if (brush) {
      brush.SetPaintParticles(paintParticles, paintParticlesBrush);
    }
  }
}
