﻿using UnityEngine;
using System.Collections;

public class PaintBrushParticles : MonoBehaviour {

  public ParticlePaint paintParticles;
  public GameObject paintParticleBrush;

  public Transform particleAttachment;

  public void SetPaintParticles(ParticlePaint paintParticles, GameObject brush) {

    if ((paintParticles == null && this.paintParticles == null) ||
      (paintParticles && this.paintParticles && this.paintParticles.SameType(paintParticles))) {
      return;
    }

    this.paintParticles = null;

    if (this.paintParticleBrush) {
        Destroy(paintParticleBrush);
    }

    if (paintParticles) {
      this.paintParticles = paintParticles;
      if (brush != null) {
        paintParticleBrush = Instantiate(brush, particleAttachment.position, particleAttachment.rotation) as GameObject;
        paintParticleBrush.transform.parent = particleAttachment;
      }
    }
  }
}
